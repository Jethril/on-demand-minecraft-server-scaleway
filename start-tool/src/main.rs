use const_str::{parse, to_byte_array, unwrap};
use dotenv_codegen::dotenv;
use native_dialog::{MessageDialog, MessageType};
use std::num::NonZeroU16;
use tokio::io::AsyncWriteExt;
use tokio::net::TcpStream;

/// Host used
const STARTUP_HOST: &str = dotenv!("STARTUP_HOST");
const STARTUP_PORT: NonZeroU16 = unwrap!(NonZeroU16::new(parse!(dotenv!("STARTUP_PORT"), u16)));
const TOKEN: [u8; 128] = to_byte_array!(dotenv!("TOKEN"));

#[tokio::main(flavor = "current_thread")]
async fn main() {
    if let Err(e) = start_instance().await {
        eprintln!("Could not start the instance: {e}");

        let _ = MessageDialog::new()
            .set_title("Erreur")
            .set_text(&format!("Impossible de démarrer le serveur.\n\nDétails :\n{e}"))
            .set_type(MessageType::Error)
            .show_alert();
    } else {
        println!("Instance started.");

        let _ = MessageDialog::new()
            .set_title("Démarrage du serveur")
            .set_text(&format!("Le serveur a été démarré"))
            .set_type(MessageType::Info)
            .show_alert();
    }
}

/// Starts the instance by sending the token to the manager.
async fn start_instance() -> anyhow::Result<()> {
    let mut connection = TcpStream::connect((STARTUP_HOST, STARTUP_PORT.get())).await?;

    connection.write_all(&TOKEN).await?;
    Ok(())
}
