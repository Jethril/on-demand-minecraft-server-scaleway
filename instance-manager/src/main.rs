use anyhow::Context;
use const_str::{format as const_format, parse, to_byte_array, unwrap};
use dotenv_codegen::dotenv;
use listener::{Listener, Token};
use log::{error, info};
use manager::Manager;
use std::num::NonZeroU16;
use std::process;
use std::time::Duration;
use systemd_journal_logger::JournalLog;

const LISTEN_PORT: NonZeroU16 = unwrap!(NonZeroU16::new(parse!(dotenv!("STARTUP_PORT"), u16)));
const MC_PORT: NonZeroU16 = unwrap!(NonZeroU16::new(parse!(dotenv!("MC_PORT"), u16)));
const SC_API_KEY: &str = dotenv!("SCW_SECRET_KEY");
const SC_ZONE: &str = dotenv!("SCW_ZONE_ID");
const SC_SERVER_ID: &str = dotenv!("SCW_INSTANCE_ID");
const SHUTDOWN_DELAY: Duration = Duration::from_secs(parse!(dotenv!("SHUTDOWN_DELAY_SECS"), u64));
const SHUTDOWN_TIME: Duration = Duration::from_secs(parse!(dotenv!("SHUTDOWN_TIME_SECS"), u64));
const TOKEN: Token = to_byte_array!(dotenv!("TOKEN"));

mod listener;
mod manager;
mod scaleway_api;

#[tokio::main(flavor = "current_thread")]
async fn main() {
    setup_logger();

    if let Err(e) = main_failible().await {
        error!("Could not start the manager: {e:?}");
        process::exit(2);
    }
}

/// Equivalent to the [`main`] function, but allows failures.
async fn main_failible() -> anyhow::Result<()> {
    info!(target: "main", "Instance manager is starting...");
    let mut manager = Manager::new(
        MC_PORT,
        SC_API_KEY.to_owned(),
        SC_ZONE.to_owned(),
        SC_SERVER_ID.to_owned(),
        SHUTDOWN_DELAY,
        SHUTDOWN_TIME,
    )
        .unwrap();

    let mut listener = Listener::new(LISTEN_PORT, TOKEN)
        .await
        .context(const_format!("Could not listen on port {}", dotenv!("STARTUP_PORT")))?;

    info!(target: "main", "Instance manager is listening to connections on port {LISTEN_PORT}.");

    loop {
        tokio::select! {
            () = listener.next_query() => manager.try_start().await,
            () = manager.do_housekeeping() => unreachable!(),
        }
    }
}

/// Sets the logging system up.
fn setup_logger() {
    #[cfg(unix)]
    if systemd_journal_logger::connected_to_journal() {
        JournalLog::default().install().unwrap();
        return;
    }

    pretty_env_logger::init_custom_env("IM_LOG");
}
