use log::debug;
use std::num::NonZeroU16;
use std::time::Duration;
use tokio::io::AsyncReadExt;
use tokio::net::TcpListener;
use tokio::time;

/// Type used to store the token.
pub type Token = [u8; 128];

/// Maximum time to wait for the token before dropping the connection.
const READ_TIMEOUT: Duration = Duration::from_secs(3);

/// Structure in charge of listening to startup queries.
pub struct Listener {
    tcp_listener: TcpListener,
    token: Token,
}

impl Listener {
    /// Creates a new listener that will listen to connections on the supplied TCP port, and verify the token.
    pub async fn new(port: NonZeroU16, token: Token) -> anyhow::Result<Self> {
        Ok(Self {
            tcp_listener: TcpListener::bind(&format!("::0:{port}")).await?,
            token,
        })
    }

    /// Listens to new server start queries and keeps blocking until a valid client request is received.
    pub async fn next_query(&mut self) {
        loop {
            debug!(target: "socket", "Waiting for connections.");

            if let Ok((mut stream, ip)) = self.tcp_listener.accept().await {
                debug!(target: "socket", "Received a connection from {ip}. Waiting for the token.");

                let mut token: Token = [0; 128];

                tokio::select! {
                    result = stream.read_exact(&mut token) => {
                        if let Err(e) = result {
                            debug!(target: "socket", "Could not read token from {ip}: {e:?}");
                        } else {
                            if token == self.token {
                                debug!(target: "socket", "Token received from {ip}");
                                return;
                            }

                            debug!(target: "socket", "Invalid token received from {ip}");
                        }
                    },

                    _ = time::sleep(READ_TIMEOUT) => {
                        debug!(target: "socket", "Connection from {ip} timed out reading the token.");
                    }
                }
            }
        }
    }
}
