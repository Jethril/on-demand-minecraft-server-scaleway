use crate::scaleway_api::{Instance, InstanceAction, InstanceState, ScalewayApiClient};
use anyhow::Context;
use chrono::{DateTime, Duration as ChronoDuration, Utc};
use log::{debug, error, info, trace, warn};
use msp::Conf;
use std::mem;
use std::num::NonZeroU16;
use std::time::Duration;
use tokio::time;

const SC_INSTANCE_OFF_CHECK_INTERVAL: Duration = Duration::from_secs(600);
const SC_INSTANCE_ON_CHECK_INTERVAL: Duration = Duration::from_secs(10);

/// Structure that contains the instance management logic.
#[derive(Debug)]
pub struct Manager {
    /// The port on which the Minecraft server is listening.
    mc_port: NonZeroU16,

    /// The Scaleway API client.
    sc_client: ScalewayApiClient,

    /// The Scaleway instance ID.
    sc_srv_id: String,

    /// The current instance state.
    state: State,

    /// The delay to wait before shutting the server down.
    shutdown_delay: Duration,

    /// The estimated time the instance takes to fully shut down.
    shutdown_time: Duration,
}

impl Manager {
    /// Creates a new instance manager.
    ///
    /// # Parameters
    /// - `mc_port`: the port on which the Minecraft server is listening. Its IP address will be deduced from the
    ///   instance metadata provided by the Scaleway API.
    /// - `sc_api_key`: the API key used to authenticate against the Scaleway API.
    /// - `sc_zone`: the Scaleway zone in which the instance is located.
    /// - `sc_srv_id`: the Scaleway instance ID.
    /// - `shutdown_delay`: the time to wait before shutting down the server. The manager won't shut the server after
    ///   this delay but will instead wait **at least** this delay before shutting it down. To maximize the already paid
    ///   resources, the manager will shut the server down once the latest billed hour finishes. This delay avoids the
    ///   edge case that consists of shutting the server down immediately when someone disconnects near the end of the
    ///   billed hour.
    /// - `shutdown_time`: the estimated time the server will take to shut the server down. The manager will consider
    ///   shutting down the instance if the remaining time before the last billed hour is superior or equal to this
    ///   time. This delay ensures that you won't be billed for a whole hour because the server was still shutting
    ///   down when the billed hour ended.
    pub fn new(
        mc_port: NonZeroU16,
        sc_api_key: String,
        sc_zone: String,
        sc_srv_id: String,
        shutdown_delay: Duration,
        shutdown_time: Duration,
    ) -> anyhow::Result<Self> {
        Ok(Self {
            mc_port,
            sc_client: ScalewayApiClient::new(&sc_api_key, &sc_zone)?,
            sc_srv_id,
            state: State::Initializing,
            shutdown_delay,
            shutdown_time,
        })
    }

    /// Runs the manager tasks (retrieving the servers' states, shutting them down etc.).
    pub async fn do_housekeeping(&mut self) -> ! {
        loop {
            debug!(target: "manager", "Started housekeeping.");

            let sleep_duration = match self.do_housekeeping_fallible().await {
                Ok(duration) => duration,
                Err(e) => {
                    warn!("An error occurred while housekeeping: {e:?}");
                    Duration::from_secs(3)
                }
            };

            debug!(target: "manager", "Finished housekeeping, now sleeping for {} secs.", sleep_duration.as_secs());
            time::sleep(sleep_duration).await;
        }
    }

    /// Attempts to start the instance.
    pub async fn try_start(&mut self) {
        info!(target: "manager", "Attempting to start the instance...");

        match self.state {
            State::Stopping { ref mut should_start } if !*should_start => {
                info!(target: "manager", "The instance is stopping, marking the restart request.");
                *should_start = true;
            }
            State::Stopped => {
                if let Err(e) = self.start_instance().await {
                    warn!(target: "manager", "An error occurred while starting the instance: {e:?}");
                } else {
                    info!(target: "manager", "The instance was successfully started.");
                }
            }
            _ => info!(target: "manager", "The instance is already running, nothing to do."),
        }
    }

    /// An equivalent to the [`do_housekeeping`] method, but allows failures.
    async fn do_housekeeping_fallible(&mut self) -> anyhow::Result<Duration> {
        trace!(target: "manager", "Retrieving the instance state...");
        let Some(instance) = self
            .sc_client
            .find_instance_by_id(&self.sc_srv_id)
            .await
            .context("Failed to retrieve the Scaleway instance state")? else {
            error!(target: "manager", "No instance with ID {} could be found, this is a configuration error!", self.sc_srv_id);
            return Ok(SC_INSTANCE_OFF_CHECK_INTERVAL);
        };

        trace!(target: "manager", "The retrieved instance was: {instance:?}");

        trace!(target: "manager", "Comparing states...");
        let mut previous_state = State::create_if_changed(&self.state, &instance, self.mc_port);

        if let Some(state) = &mut previous_state {
            mem::swap(&mut self.state, state);
            trace!(target: "manager", "The states changed. Previous was: {previous_state:?}, current is: {:?}", self.state);
        } else {
            trace!(target: "manager", "The states did not change.");
        }

        let (start_date, mc_server, empty_since) = match &mut self.state {
            State::Starting { .. } => {
                trace!(target: "manager", "The instance is still starting, nothing to do.");
                return Ok(SC_INSTANCE_ON_CHECK_INTERVAL);
            }

            State::Started {
                start_date,
                mc_server,
                empty_since,
            } => (*start_date, mc_server, empty_since), // Proceed.

            State::Stopping { .. } => {
                trace!(target: "manager", "The instance is still stopping, nothing to do.");
                return Ok(SC_INSTANCE_ON_CHECK_INTERVAL);
            }

            State::Stopped if matches!(previous_state, Some(State::Stopping { should_start: true })) => {
                debug!(target: "manager", "The instance was stopping, re-starting it as requested.");

                self.start_instance()
                    .await
                    .context("Failed to start the Scaleway instance")?;

                return Ok(SC_INSTANCE_ON_CHECK_INTERVAL);
            }

            State::Stopped => {
                trace!(target: "manager", "The instance is stopped, nothing to do.");
                return Ok(SC_INSTANCE_OFF_CHECK_INTERVAL);
            }

            State::Initializing => unreachable!(),
        };

        trace!(target: "manager", "The instance is online, retrieving the Minecraft server info...");
        let server_info = mc_server
            .get_server_status()
            .context("Failed to retrieve the Minecraft server info")?;
        let now = Utc::now();

        match *empty_since {
            None if server_info.players.online == 0 => {
                trace!(target: "manager", "No players are online, scheduling the instance shutdown.");

                *empty_since = Some(now);
                return Ok(SC_INSTANCE_ON_CHECK_INTERVAL);
            }

            Some(_) if server_info.players.online > 0 => {
                trace!(target: "manager", "Players are online, aborting the instance shutdown.");

                *empty_since = None;
                return Ok(SC_INSTANCE_ON_CHECK_INTERVAL);
            }

            Some(date) if (now - date).to_std()? >= self.shutdown_delay => (), // Proceed.

            _ => {
                trace!(target: "manager", "The Minecraft server is running and nothing is to be done, idling.");
                return Ok(SC_INSTANCE_ON_CHECK_INTERVAL);
            }
        }

        let deadline = start_date + ChronoDuration::hours((now - start_date).num_hours() + 1) - self.shutdown_time;
        trace!(target: "manager", "The instance shutdown is planned at {deadline}.");

        if now >= deadline {
            trace!(target: "manager", "We're behind the planned shutdown date, stopping the instance...");
            self.stop_instance()
                .await
                .context("Failed to stop the Scaleway instance")?;

            trace!(target: "manager", "Instance stopped.");
        }

        Ok(SC_INSTANCE_ON_CHECK_INTERVAL)
    }

    /// Starts the Scaleway instance.
    async fn start_instance(&mut self) -> anyhow::Result<()> {
        info!(target: "manager", "Starting the instance...");
        self.sc_client
            .perform_action(&self.sc_srv_id, InstanceAction::PowerOn)
            .await?;

        info!(target: "manager", "The instance has been started.");

        Ok(())
    }

    /// Stops the Scaleway instance.
    async fn stop_instance(&mut self) -> anyhow::Result<()> {
        info!(target: "manager", "Stopping the instance...");
        self.sc_client
            .perform_action(&self.sc_srv_id, InstanceAction::PowerOff)
            .await?;

        info!(target: "manager", "The instance has been stopped.");

        Ok(())
    }
}

/// Represents the different states a managed instance can have.
#[derive(Debug, Clone)]
enum State {
    /// The manager is initializing and both the instance and the server state are unknown yet.
    Initializing,

    /// The instance is starting.
    Starting { start_date: DateTime<Utc> },

    /// The instance is started and the Minecraft server is running.
    Started {
        /// The date at which the server started (used to know when we should power the instance off).
        start_date: DateTime<Utc>,

        /// The Minecraft server ping configuration.
        mc_server: Conf,

        /// The date at which the Minecraft server was first seen empty (without any player connected). The value is set
        /// at startup and each time the last player leaves the server.
        empty_since: Option<DateTime<Utc>>,
    },

    /// The instance is stopping.
    Stopping {
        /// A boolean indicating whether the server should start after it has been stopped (allows registering
        /// concurrent requests).
        should_start: bool,
    },

    /// The instance is stopped.
    Stopped,
}

impl State {
    /// Creates a new state from the supplied elements if and only if the state differs from the previous one.
    ///
    /// # Parameters
    /// - `previous`: the previous state,
    /// - `instance`: the Scaleway instance data,
    /// - `mc_port`: the Minecraft server port.
    fn create_if_changed(previous: &State, instance: &Instance, mc_port: NonZeroU16) -> Option<Self> {
        /// Little shortcut to make the rest of the code less verbose.
        type IS = InstanceState;

        /// Creates a Minecraft server.
        fn create_mc_server(instance: &Instance, mc_port: NonZeroU16) -> Conf {
            Conf::create_with_port(&instance.public_ip.address.to_string(), mc_port.get())
        }

        match (previous, instance.state) {
            // Cases for which the states are equal:
            (Self::Stopping { .. }, IS::Stopping | IS::StoppedInPlace | IS::Locked) => None,
            (Self::Stopped, IS::Stopped) => None,
            (Self::Starting { .. }, IS::Starting) => None,
            (Self::Started { mc_server, .. }, IS::Running)
                if mc_server.host == instance.public_ip.address.to_string() =>
            {
                None
            }

            // Cases for which the switching requires a transition:
            (Self::Starting { start_date }, IS::Running) => Some(Self::Started {
                start_date: *start_date,
                mc_server: create_mc_server(instance, mc_port),
                empty_since: None,
            }),

            // Other cases:
            (_, IS::Stopping) => Some(Self::Stopping { should_start: false }),
            (_, IS::Stopped | IS::StoppedInPlace | IS::Locked) => Some(Self::Stopped),

            (_, IS::Starting) => Some(Self::Starting {
                start_date: instance.modification_date,
            }),

            (_, IS::Running) => Some(Self::Started {
                start_date: instance.modification_date,
                mc_server: create_mc_server(instance, mc_port),
                empty_since: None,
            }),
        }
    }
}
