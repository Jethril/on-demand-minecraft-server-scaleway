use chrono::{DateTime, Utc};
use reqwest::header::{HeaderMap, HeaderValue};
use reqwest::{Client, StatusCode, Url};
use serde::{Deserialize, Serialize};
use serde_with::{DeserializeFromStr, SerializeDisplay};
use std::net::IpAddr;

/// Convenience macro that builds a URL from the base URL provided in the [`ScalewayApiClient`] structure.
macro_rules! url {
    ($self:expr, $($segment:expr),+) => ({
        let mut url = $self.base_url.clone();

        {
            let mut segments = url.path_segments_mut().unwrap();
            $(segments.push($segment);)+
        }

        url
    })
}

/// Structure that represents a scaleway API client.
#[derive(Debug)]
pub struct ScalewayApiClient {
    http_client: Client,
    base_url: Url,
}

impl ScalewayApiClient {
    /// Creates a new Scaleway API client.
    ///
    /// # Parameters
    /// - `secret`: the Scaleway API key.
    /// - `zone_id`: the Scaleway zone.
    ///
    /// # Errors
    /// If the secret contains illegal characters for an HTTP header.
    pub fn new(secret: &str, zone_id: &str) -> anyhow::Result<Self> {
        let mut base_url = Url::parse("https://api.scaleway.com/instance/v1/zones").unwrap();
        base_url.path_segments_mut().unwrap().push(zone_id);

        let mut headers = HeaderMap::new();
        headers.insert("X-Auth-Token", HeaderValue::from_str(secret)?);

        Ok(Self {
            http_client: Client::builder().default_headers(headers).build()?,
            base_url,
        })
    }

    /// Attempts to find an instance by ID.
    pub async fn find_instance_by_id(&self, id: &str) -> anyhow::Result<Option<Instance>> {
        #[derive(Debug, Deserialize)]
        struct Response {
            server: Instance,
        }

        let http_response = self.http_client.get(url!(self, "servers", id)).send().await?;

        if http_response.status() == StatusCode::NOT_FOUND {
            Ok(None)
        } else {
            Ok(Some(http_response.json::<Response>().await?.server))
        }
    }

    /// Runs the supplied action on the given instance.
    pub async fn perform_action(&self, srv_id: &str, action: InstanceAction) -> anyhow::Result<()> {
        #[derive(Debug, Serialize)]
        struct Request {
            action: InstanceAction,
        }

        self.http_client
            .post(url!(self, "servers", srv_id, "action"))
            .json(&Request { action })
            .send()
            .await?;

        Ok(())
    }
}

#[derive(Debug, Deserialize)]
pub struct Instance {
    pub public_ip: PublicIpInfo,
    pub modification_date: DateTime<Utc>,
    pub state: InstanceState,
}

#[derive(Debug, Deserialize)]
pub struct PublicIpInfo {
    pub address: IpAddr,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, strum::EnumString, DeserializeFromStr)]
pub enum InstanceState {
    #[strum(serialize = "running")]
    Running,

    #[strum(serialize = "stopped")]
    Stopped,

    #[strum(serialize = "stopped in place")]
    StoppedInPlace,

    #[strum(serialize = "starting")]
    Starting,

    #[strum(serialize = "stopping")]
    Stopping,

    #[strum(serialize = "locked")]
    Locked,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, strum::Display, SerializeDisplay)]
pub enum InstanceAction {
    #[strum(serialize = "poweron")]
    PowerOn,

    #[strum(serialize = "poweroff")]
    PowerOff,
}
